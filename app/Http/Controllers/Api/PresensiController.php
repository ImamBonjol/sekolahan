<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\Presensi;
use App\Model\Kelas;
use App\Model\Siswa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class PresensiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Presensi::with(['siswa'])->get();

        return $this->success($data, 200);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'siswa_id' => 'required',
            'status' => 'required|in:present,absent,execused,late',
            'keterangan' => 'required|string'
        ]);

        if($validator->fails()){
            $msg = $validator->errors();

            return $this->failedResponse($msg, 422);
        }

        $presensi = new Presensi();
        $presensi->siswa_id = $request->siswa_id;
        $presensi->status = $request->status;
        $presensi->keterangan = $request->keterangan;
        $saved = $presensi->save();
        if($saved){
            return $this->success($presensi, 201);
        } else {
            return $this->failedResponse('Presensi gagal ditambahkan!', 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Presensi  $presensi
     * @return \Illuminate\Http\Response
     */
    public function show(Presensi $presensi)
    {
        return $this->success($presensi, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Presensi  $presensi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Presensi $presensi)
    {
        $validator = Validator::make($request->all(), [
            'siswa_id' => 'required',
            'status' => 'required|in:present,absent,execused,late',
            'keterangan' => 'required|string'
        ]);

        if($validator->fails()){
            $msg = $validator->errors();

            return $this->failedResponse($msg, 422);
        }

        $presensi->siswa_id = $request->siswa_id;
        $presensi->status = $request->status;
        $presensi->keterangan = $request->keterangan;
        $saved = $presensi->save();
        if($saved){
            return $this->success($presensi, 201);
        } else {
            return $this->failedResponse('Presensi gagal ditambahkan!', 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Presensi  $presensi
     * @return \Illuminate\Http\Response
     */
    public function destroy(Presensi $presensi)
    {
        $delete = $presensi->delete();

        if($delete){
            return $this->success(null, 200);
        } else {
            return $this->failedResponse('Gagal Menghapus Presensi', 500);
        }
    }

    public function presensi($status){
        $data = Presensi::join('siswa','siswa.kelas_id','=','presensi.siswa_id')
                        ->join('kelas','kelas.id','=','siswa.kelas_id')
                        ->join('jadwal','jadwal.kelas_id','=','kelas.id')
                        ->join('mapel','mapel.id','=','jadwal.mapel_id')
                        ->select([
                            'jadwal.hari',
                            'jadwal.jam_pelajaran',
                            'siswa.nama',
                            'siswa.nis',
                            'kelas.nama_kelas',
                            'mapel.nama_mapel',
                            'presensi.status',
                            'presensi.keterangan',
                        ])
                        ->where('status','=',$status)->get();

        return $this->success($data, 200);
    }

    public function absensi($id)
    {
        $data = Siswa::join('kelas','kelas.id','=','siswa.kelas_id')
                    ->join('jadwal','jadwal.kelas_id','=','kelas.id')
                    ->join('mapel','mapel.id','=','jadwal.mapel_id')
                    ->where('jadwal.id','=', $id)
                    ->select([
                        'siswa.id',
                        'siswa.nama',
                        'siswa.nis',
                        'kelas.nama_kelas',
                        'mapel.nama_mapel',                        
                    ])
                    ->get();
        
        return $this->success($data, 200);
    }

    private function success($data,$statusCode,$message='success')
    {
        return response()->json([
            'status' => true,
            'message' => $message,
            'data' => $data,
            'status_code' => $statusCode,
            ],$statusCode);
    }

    private function failedResponse($message,$statusCode)
    {
        return response()->json([
            'status' => false,
            'message' => $message,
            'data' => null,
            'status_code' => $statusCode,
            ],$statusCode);
    }
}
