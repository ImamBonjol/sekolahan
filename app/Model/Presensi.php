<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Jadwal;

class Presensi extends Model
{
    protected $table = 'presensi';
    protected $guarded = ['id'];

    public function siswa(){
        return $this->belongsTo(Siswa::class ,'siswa_id');
    }
}
